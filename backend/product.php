<?php
include "conn.php";
?>



<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add Product</title>
    <!-- Bootstrap CSS -->
    <link href="bootstarp/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="alert alert-primary h4 text-center mb-4 mt-4" role="alert">
                    เพิ่มข้อมูลสินค้า
                </div>
                <form class="form" action="insert_product.php" method="post" enctype="multipart/form-data"> <br>
                    <label>รหัสสินค้า:</label>
                    <input type="text" name="pid" class="form-control" placeholder="รหัสสินค้า..." require> <br>

                    <label>ชื่อสินค้า:</label>
                    <input type="text" name="pname" class="form-control" placeholder="ชื่อสินค้า..." require> <br>

                    <label>ประเภทสินค้า:</label>
                    <select class="form-select" name="typeid">
                        <?php
                        $sql = "SELECT * FROM TYPE ORDER BY TYPE_NAME";
                        $hand = oci_parse($conn, $sql);
                        oci_execute($hand);
                        while ($row = oci_fetch_array($hand)) { //check ว่ามีกี่ reccod
                        ?>
                            <option value="<?= $row['TYPE_ID'] ?>"><?= $row['TYPE_NAME'] ?></option>
                        <?php
                        }
                        ?>
                    </select> <br>

                    <label>วันที่:</label>
                    <input type="date" name="pdate" class="form-control" require> <br>

                    <label>บริษัทคู่ค้า:</label>
                    <select class="form-select" name="partnerid">
                        <?php
                        $sql = "SELECT * FROM PARTNER ";
                        $hand = oci_parse($conn, $sql);
                        oci_execute($hand);
                        while ($row = oci_fetch_array($hand)) { //check ว่ามีกี่ reccod
                        ?>
                            <option value="<?= $row['PARTNER_ID'] ?>"><?= $row['PARTNER_NAME'] ?></option>
                        <?php
                        }
                        ?>
                    </select><br>

                    <label>แบรนด์:</label>
                    <select class="form-select" name="brandid">
                        <?php
                        $sql = "SELECT * FROM BRAND ORDER BY BRAND_NAME";
                        $hand = oci_parse($conn, $sql);
                        oci_execute($hand);
                        while ($row = oci_fetch_array($hand)) { //check ว่ามีกี่ reccod
                        ?>
                            <option value="<?= $row['BRAND_ID'] ?>"><?= $row['BRAND_NAME'] ?></option>
                        <?php
                        }
                        ?>
                    </select><br>

                    <label>ซีซั่น:</label>
                    <select class="form-select" name="seasonid">
                        <?php
                        $sql = "SELECT * FROM SEASON ORDER BY SEASON_NAME";
                        $hand = oci_parse($conn, $sql);
                        oci_execute($hand);
                        while ($row = oci_fetch_array($hand)) { //check ว่ามีกี่ reccod
                        ?>
                            <option value="<?= $row['SEASON_ID'] ?>"><?= $row['SEASON_NAME'] ?></option>
                        <?php
                        }
                        ?>
                    </select><br>

                    <label>ไซส์:</label>
                    <select class="form-select" name="sizeid">
                        <?php
                        $sql = "SELECT * FROM SIZEPRODUCT";
                        $hand = oci_parse($conn, $sql);
                        oci_execute($hand);
                        while ($row = oci_fetch_array($hand)) { //check ว่ามีกี่ reccod
                        ?>
                            <option value="<?= $row['ID'] ?>"><?= $row['NAME'] ?></option>
                        <?php
                        }
                        ?>
                    </select><br>

                    <label>สี:</label>
                    <select class="form-select" name="colorid">
                        <?php
                        $sql = "SELECT * FROM COLOR ";
                        $hand = oci_parse($conn, $sql);
                        oci_execute($hand);
                        while ($row = oci_fetch_array($hand)) { //check ว่ามีกี่ reccod
                        ?>
                            <option value="<?= $row['COLOR_ID'] ?>"><?= $row['COLOR_NAME'] ?></option>
                        <?php
                        }
                        oci_close($conn);
                        ?>
                    </select><br>

                    <label>ราคา:</label>
                    <input type="number" name="price" class="form-control" placeholder="ราคา..." require> <br>
                    <label>จำนวน:</label>
                    <input type="number" name="qty" class="form-control" placeholder="จำนวน..." require> <br>
                    <label>รูปภาพ:</label>
                    <input type="file" name="image" class="form-control" require> <br>

                    <input type="submit" class="btn btn-primary" name="submit" value="submit">
                    <a class="btn btn-danger" href="show_product.php" role="button">cancel</a>
                </form><br>
            </div>
        </div>
    </div>
</body>

</html>