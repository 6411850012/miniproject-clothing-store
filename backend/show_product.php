<?php
include "conn.php";
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Show Product</title>
    <!-- Bootstrap CSS -->
    <link href="bootstarp/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
    <div class="container">
        <div class="alert alert-primary h4 text-center mb-4 mt-4" role="alert">
            แสดงข้อมูลสินค้า
        </div>
        <a class="btn btn-primary mb-4" href="product.php" role="button">Add+</a>
        <table class="table table-hover">
            <tr>
                <th>รหัสสินค้า</th>
                <th>ชื่อสินค้า</th>
                <th>ประเภทสินค้า</th>
                <th>วันที่</th>
                <th>บริษัทคู่ค้า</th>
                <th>แบรนด์</th>
                <th>ซีซั่น</th>
                <th>ไซส์</th>
                <th>สี</th>
                <th>ราคา</th>
                <th>จำนวน</th>
                <th>รูปภาพ</th>
            </tr>
            <?php
            $sql = "SELECT PRODUCT_ID,PRODUCT_NAME,TYPE_NAME,PRODUCT_DATE,PARTNER_NAME,BRAND_NAME,SEASON_NAME,
            NAME,COLOR_NAME,SELL,QUANTITY,IMAGE
            FROM PRODUCT,PRODUCTDETAIL,TYPE,PARTNER,BRAND,SEASON,COLOR,SIZEPRODUCT
            WHERE (PRODUCT_ID=PRODUCTDETAIL.PROID AND PRODUCT.PRODUCT_TYPEID=TYPE_ID AND 
            PRODUCT.PRODUCT_BRANDID = BRAND_ID AND PRODUCT.PRODUCT_COLORID = COLOR_ID AND
            PRODUCT.PRODUCT_PARTNERID=PARTNER_ID AND PRODUCT.PRODUCT_SEASONID=SEASON_ID AND
            PRODUCTDETAIL.SIZEID=SIZEPRODUCT.ID) ";
            $hand = oci_parse($conn, $sql);
            oci_execute($hand);
            while ($row = oci_fetch_array($hand)) {
            ?>
                <tr>
                    <td><?= $row['PRODUCT_ID'] ?> </td>
                    <td><?= $row['PRODUCT_NAME'] ?> </td>
                    <td><?= $row['TYPE_NAME'] ?> </td>
                    <td><?= $row['PRODUCT_DATE'] ?> </td>
                    <td><?= $row['PARTNER_NAME'] ?> </td>
                    <td><?= $row['BRAND_NAME'] ?> </td>
                    <td><?= $row['SEASON_NAME'] ?> </td>
                    <td><?= $row['NAME'] ?> </td>
                    <td><?= $row['COLOR_NAME'] ?> </td>
                    <td><?= $row['SELL'] ?> </td>
                    <td><?= $row['QUANTITY'] ?> </td>
                    <td><img src="image/<?= $row['IMAGE'] ?>" width="80px" height="100px"> </td>
                </tr>
            <?php
            }
            oci_close($conn);
            ?>
        </table>
    </div>
</body>

</html>